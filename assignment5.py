#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    return json.dumps(books)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)


	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new = {}
        firstId = -1
        for i in range(len(books)):
            if(books[i]['id'] != str(i + 1)):
                firstId = i + 1
                break
        if firstId == -1:
            firstId = len(books) + 1
        newName = request.form['name']
        new['title'] = newName
        new['id'] = str(firstId)
        books.insert(firstId - 1, new)
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    for book in books:
        if book['id'] == str(book_id):
            edit = book
    if request.method == 'POST':
        newName = request.form['name']
        edit['title'] = newName
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', title=edit['title'], book_id = book_id)

	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    for book in books:
        if book['id'] == str(book_id):
            delete = book
    if request.method == 'POST':
        for book in books:
            if book['id'] == str(book_id):
                books.remove(book)
        return redirect(url_for('showBook'))
    else:
        return render_template('deleteBook.html',title=delete['title'], book_id = book_id)


if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

